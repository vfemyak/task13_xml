<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: #33cc33;
                    text-align:center;
                    }

                    th {
                    background-color: #339900;
                    }

                </style>
            </head>

            <body>
                <table class="tfmt">
                    <tr>
                        <th style="width:250px">Type</th>
                        <th style="width:350px">One handed?</th>
                        <th style="width:250px">Origin</th>
                        <th style="width:250px">Collectible?</th>
                        <th style="width:250px">Blade length</th>
                        <th style="width:250px">Blade width</th>
                        <th style="width:250px">Edge material</th>
                        <th style="width:250px">Handle material</th>
                        <th style="width:250px">Bloodstream?</th>
                    </tr>

                    <xsl:for-each select="knifes/knife">
                        <xsl:sort select="type"/>

                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="type" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="one_handed" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="origin" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="value" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="visual/blade/length" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="visual/blade/width" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="visual/material/edge" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="visual/material/handle" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="visual/bloodstream" />
                            </td>

                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>