package parser;

import models.Knife;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\XML_files\\knifesXML.xml");
        File xsd = new File("src\\main\\resources\\XML_files\\knifesXSD.xsd");

//        if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
//            printList(SAXParserUser.parseBeers(xml, xsd), "SAX");

//            StAX
            printList(StAXReader.parseKnifes(xml, xsd), "StAX");

//            DOM
//            printList(DOMParserUser.getBeerList(xml, xsd), "DOM");
    }
//
//    private static boolean checkIfXSD(File xsd) {
//        return ExtensionChecker.isXSD(xsd);
//    }
//
//    private static boolean checkIfXML(File xml) {
//        return ExtensionChecker.isXML(xml);
//    }

    private static void printList(List<Knife> knifes, String parserName) {
        //Collections.sort(beers, new BeerComparator());
        System.out.println(parserName);
        for (Knife knife : knifes) {
            System.out.println(knife);
        }
    }
}
