package parser;

import models.*;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StAXReader {
    public static List<Knife> parseKnifes (File xml, File xsd){
        List<Knife> knifeList = new ArrayList<Knife>();
        Knife knife = null;
        Visual visual = null;
        Material material = null;
        Blade blade = null;
        HandleType handleType = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();

                    switch (name) {
                        case "knife":
                            knife = new Knife();

                            Attribute idAttr = startElement.getAttributeByName(new QName("knifeID"));
                            if (idAttr != null) {
                                knife.setKnifesID(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "one_handed":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setOne_handed(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert knife != null;
                            knife.setValue(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "visual":
                            xmlEvent = xmlEventReader.nextEvent();
                            visual = new Visual();
                            break;
                        case "blade":
                            xmlEvent = xmlEventReader.nextEvent();
                            blade = new Blade();
                            break;
                        case "length":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert blade != null;
                            blade.setLength(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "width":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert blade != null;
                            blade.setWidth(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "material":
                            xmlEvent = xmlEventReader.nextEvent();
                            material = new Material();
                            break;
                        case "edge":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert material != null;
                            material.setEdge(xmlEvent.asCharacters().getData());
                            break;
                        case "handle":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert material != null;
                            material.setHandle(HandleType.valueOf(xmlEvent.asCharacters().getData().toUpperCase()));
                            break;
                        case "bloodstream":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visual != null;
                            visual.setBloodstream(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));

                            xmlEvent = xmlEventReader.nextEvent();
                            assert visual != null;
                            visual.setBlade(blade);
                            assert visual != null;
                            visual.setMaterial(material);
                            assert knife != null;
                            knife.setVisual(visual);
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("knife")){
                        knifeList.add(knife);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return knifeList;
    }
}
