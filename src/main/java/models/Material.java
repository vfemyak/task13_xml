package models;

public class Material {
    private String edge;
    private HandleType handle;

    public Material() {
    }

    public Material(String edge, HandleType handle) {
        this.edge = edge;
        this.handle = handle;
    }

    public String getEdge() {
        return edge;
    }

    public void setEdge(String edge) {
        this.edge = edge;
    }

    public HandleType getHandle() {
        return handle;
    }

    public void setHandle(HandleType handle) {
        this.handle = handle;
    }

    @Override
    public String toString() {
        return "Material{" +
                "edge='" + edge + '\'' +
                ", handle=" + handle +
                '}';
    }
}
